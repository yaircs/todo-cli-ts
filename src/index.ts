#!/usr/bin/env node
import fs from 'fs/promises';
import readline from 'readline';
// import log from '@ajar/marker';

type stateObject = {
    tasksCounter: number;
    tasks: taskObject[];
}

type taskObject = {
    id: number,
    isCompleted: string,
    title: string
}

type commandsObject = {
    [command: string]: Function;
}

const rl: readline.Interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


async function save(data :object={}, file: string='data.json'): Promise<void> {
    // async write file to json
    await fs.writeFile('data.json', JSON.stringify(data, null, 2));
}

async function load(file: string='data.json'): Promise<stateObject>  { //check fix
    // async load of data, if file is empty return an empty object
    const dirList: string[] = await fs.readdir('./');
    let text: string = '';
    if (dirList.includes('data.json')) text = await fs.readFile(file, 'utf-8');
    
    return JSON.parse(
        text.length ? text : `{}`
        )  
}

    
async function generateId (): Promise<number> {
    // find max id and return the next one (+1)
    let state: stateObject = await load();  //check fix

    let nextId: number = 0;
    state.tasks.forEach(task => {
        if (task.id > nextId) nextId = task.id;
    })
    nextId += 1;

    return nextId;
}

async function isIdExsits (id: string): Promise<boolean> {
    // checks if input id param is a valid id
    const state: stateObject = await load();

    if (state.tasks.some(task => task.id === Number(id))) return true;
    else {
        console.log(`Error: Task ID '${id}' not found`);
        return false;
    }
}

function help(): void {
    console.log('\n');
    console.log('Available commands are:');
    console.log('help')
    for(let action in actions) console.log(`${action}`);
  }

// actions functions object
const actions: commandsObject = {
    add: async function (...title: any): Promise<void> {
        // load data, generate next id, push item, increment counter and save data
        let state: stateObject = await load();
        const nextId = await generateId(); //check fix

        state.tasks.push({
            id: nextId,
            isCompleted: '☐',
            title: title.join(' '),
        });
        state.tasksCounter++;
        save(state);
    },

    'add-mul': async function (): Promise<void> {
        
        console.log(`\nAdd Multilpe tasks -> \n(press Ctrl+C to Exit)\n`)
        rl.setPrompt('Enter a new task title ->  ')
        rl.prompt();

        rl.on('line', function (line: string) {
            line = line.trim();
            actions['add'](line);
            rl.prompt();
        }).on('close', function () {
            process.exit(0);
        });
            
    },

    delete: async function (id: string): Promise<void> {
        // load data, check if id is valid, filter data, decrement counter and save data
        let state: stateObject = await load();
        if (await isIdExsits(id)){ 
            state.tasks = state.tasks.filter(task => task.id !== Number(id))  
            state.tasksCounter = state.tasksCounter-1;
            save(state);  
        }
        else return;
    },

    check: async function (id: string): Promise<void> {
        // load data, find the right task and change it's isCompleted prop to true and save data
        let state: stateObject = await load();

        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.isCompleted = '✔': null);
        else return;

        save(state);
    },

    uncheck: async function (id: string): Promise<void> {
        // load data, find the right task and change it's isCompleted prop to false and save data
        let state: stateObject = await load();

        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.isCompleted = '☐': null);
        else return;

        save(state);
    },

    update: async function (id: string, ...newTitle: string[]): Promise<void> {
            // load data, find the right task and change it's title prop to new input title and save data
        let state: stateObject = await load();
        
        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.title = newTitle.join(' ') : null)
        else return;

        save(state);
    },

    list: async function (): Promise<void> {
        // load data and print
        const state: stateObject = await load();
        if (!state.tasks.length) console.log(`Task list is empty\nUse the add command to add new tasks`);
        else console.table(state.tasks); 
    }
  
};

// IFFE init
(async function init (): Promise<void> {
    // load data, if data is empty create template
    const state: stateObject = await load();
    if(!state.tasksCounter) await save({tasksCounter: 0, tasks: []});

    // if a param\command has been passed via cli
    if (process.argv[2]) {
        // seperate command and the rest of the args
        const [command, ...args] = [ ...process.argv[2].split(' '), ...process.argv.slice(3) ]

        // fire action with ...args if such command exists
        if (actions[command]) actions[command](...args);
        // if such command dont exsits show help menu
        else help();
        
    } else help(); // if no command has been passed show help menu
    return;
 })();
